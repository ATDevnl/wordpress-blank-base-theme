<?php get_header(); ?>

<div class="container">
  <div class="card">
    <div class="card-header">
      I'm happy I could help.
    </div>
    <div class="card-body">
      <h5 class="card-title">Thank you for using the ATDEV blank base theme.</h5>
      <p class="card-text">I created this theme to make my own work process faster. It is normally a chore to set a theme, but with this theme you only need to ajust the style.css and you can start development. This blank base theme is free, always!</p>
    </div>
  </div>
  <br>
  <div class="card">
    <div class="card-header">
      Featured - What we are using.
    </div>
    <div class="card-body">
      <h5 class="card-title">Bootstrap</h5>
      <p class="card-text">Bootstrap is a life saver. Thats why i've installed bootstrap in this theme! To learn more abouth bootstrap please visit <a href="https://getbootstrap.com">getbootstrap.com</a>. </p>

      <h5 class="card-title">jQuery</h5>
      <p class="card-text">jQuery. Write less, do more. jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. To learn more abouth jQuery, please visit <a href="https://jquery.com">jQuery.com</a>. </p>

      <h5 class="card-title">Sass ready</h5>
      <p class="card-text">You can develop this theme with sass, its not required but it is handy. To learn more abouth sass, please visit <a href="https://sass-lang.com/">sass-lang.com</a>. <br> <i>Please note: this theme is Sass ready, this means that you should create and install your own workflow.</i> </p>
    </div>
  </div>
</div>
<?php get_footer(); ?>
