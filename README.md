Thank you for using the ATDEV blank base theme.
I created this theme to make my own work process faster. It is normally a chore to set a theme, but with this theme you only need to ajust the style.css and you can start development. This blank base theme is free, always!


Featured - What we are using.
Bootstrap
Bootstrap is a life saver. Thats why i've installed bootstrap in this theme! To learn more abouth bootstrap please visit getbootstrap.com.

jQuery
jQuery. Write less, do more. jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. To learn more abouth jQuery, please visit jQuery.com.

Sass ready
You can develop this theme with sass, its not required but it is handy. To learn more abouth sass, please visit sass-lang.com.
Please note: this theme is Sass ready, this means that you should create and install your own workflow.