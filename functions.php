<?php
/**
 * Enqueue scripts and styles
 */
function wpbase_scripts() {

  /**
  * Stylesheets
  */
  wp_enqueue_style( 'bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
  wp_enqueue_style( 'custom-style', get_stylesheet_uri() );
  wp_enqueue_style( 'base-style', get_template_directory_uri() . '/style/style.css' );
  wp_enqueue_style( 'base-style-map', get_template_directory_uri() . '/style/style.css.map' );

  /**
  * Scripts
  */
  wp_enqueue_script( 'popper-script', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array( 'jquery' ), '1.16.0', true );
  wp_enqueue_script( 'bootstrap-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array( 'jquery' ), '4.4.1 ', true );
  wp_enqueue_script( 'base-script', get_template_directory_uri()  . '/js/script.js', array( 'jquery' ), '0.0.1 ', true );

}

add_action( 'wp_enqueue_scripts', 'wpbase_scripts' );
