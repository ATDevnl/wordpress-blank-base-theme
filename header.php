<!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">
    <title>

      <?php
      echo bloginfo(name);
      echo ' | ';
      if (is_front_page()) {
        echo bloginfo(description);
      } else {
        echo get_the_title();
      }
      ?>
    </title>
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>
    <div class="container text-center">
      <h1> <a href="<?php echo get_site_url(); ?>"><?php bloginfo(name); ?></a> </h1>
      <h4> <?php bloginfo(description); ?> </h4>
      <p>Welcome to the ATDEV blank base theme.</p>
    </div>
